import dans.MakeIndexes;

public class TryIt {

 /**
  * This main lets you try out the MakeIndexes class.
  * It will print the index search patterns for the all squares.
  * @param args
  */
 public static void main(String[] args) {
        for (int sq = 0; sq < 81; ++sq)
            System.out.println(new MakeIndexes(sq).get());
 }

}
